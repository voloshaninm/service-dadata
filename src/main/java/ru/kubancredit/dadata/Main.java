package ru.kubancredit.dadata;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kubancredit.dadata.config.SpringConfiguration;
import ru.kubancredit.dadata.service.MyService;
import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        MyService myService = context.getBean("myService", MyService.class);
        myService.displaySetFoundStreets(myService.getSetFoundStreets());
    }
}
