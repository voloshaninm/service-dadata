package ru.kubancredit.dadata.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfProperties {
    protected static FileInputStream fileInputStream;
    protected static Properties PROPERTIES;
    static {
        try{
            //Путь для работы кода в IDE
            fileInputStream = new FileInputStream(
                    "src/main/resources/application.properties");

            //Путь для создания артефакта с рядом лежащим файлом настроек
//            fileInputStream = new FileInputStream(
//                    "./application.properties");

            PROPERTIES = new Properties();
            PROPERTIES.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fileInputStream != null)
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
    public static String getProperty(String key) {
        return PROPERTIES.getProperty(key);
    }
}

