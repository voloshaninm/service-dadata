package ru.kubancredit.dadata.config;

import com.google.gson.JsonParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ru.kubancredit.dadata.tools.MyHttpConnector;
import java.io.*;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;

@Configuration
@ComponentScan("ru.kubancredit.dadata")
@PropertySource("application.properties")
public class SpringConfiguration {

    @Bean
    public HttpURLConnection connection() throws IOException {
    return MyHttpConnector.getConnection(ConfProperties.getProperty("url"));
    }

    @Bean
    public BufferedReader reader() {
        return new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
    }

    @Bean
    public JsonParser jsonParser() {
        return new JsonParser();
    }

    @Bean
    public OutputStreamWriter outputStreamWriter(HttpURLConnection connection) throws IOException {
        return new OutputStreamWriter(connection.getOutputStream(),  StandardCharsets.UTF_8);
    }

    @Bean
    public StringBuilder stringBuilder() {
        return new StringBuilder();
    }

    @Bean
    public PrintWriter printWriter() {
        return new PrintWriter(System.out, true, StandardCharsets.UTF_8);
    }

}
