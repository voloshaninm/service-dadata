package ru.kubancredit.dadata.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.*;
import java.net.HttpURLConnection;
import java.util.HashSet;
import java.util.Set;

@Component
public class MyService {
    private HttpURLConnection connection;
    private JsonParser jsonParser;
    private BufferedReader reader;
    private OutputStreamWriter outputStreamWriter;
    private StringBuilder stringBuilder;
    private PrintWriter printWriter;

    @Autowired
    public MyService(HttpURLConnection connection, JsonParser jsonParser, BufferedReader reader,
                     OutputStreamWriter outputStreamWriter, StringBuilder stringBuilder,
                     PrintWriter printWriter) {
        this.connection = connection;
        this.jsonParser = jsonParser;
        this.reader = reader;
        this.outputStreamWriter = outputStreamWriter;
        this.stringBuilder = stringBuilder;
        this.printWriter = printWriter;
    }

    public void displaySetFoundStreets(Set<String> listStreets){
        printWriter.println("Количество найденных улиц: " + listStreets.size());
        printWriter.println("Список найденных улиц: ");
        for(String street : listStreets){
            printWriter.println(street);
        }
    }

    public Set<String> getSetFoundStreets() throws IOException {
        sendRequestToServer();
        return getSetResponseFromServer();
    }


    private void sendRequestToServer(){
        printWriter.println("Введите название улицы/фрагмент названия улицы:");
        String inputStreet = null;
        try {
            inputStreet = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String jsonInputString = "{ \"query\": \"Краснодар " + inputStreet + "\" }";
        JsonObject jsonObject = (JsonObject)jsonParser.parse(jsonInputString);

        try {
           outputStreamWriter.write(jsonObject.toString());
           outputStreamWriter.flush();
       } catch (IOException e) {
           e.printStackTrace();
       }
    }

    private Set<String> getSetResponseFromServer() throws IOException {
        Set<String> listStreets = new HashSet<>();
        try(BufferedReader br = new BufferedReader(
                                            new InputStreamReader(connection.getInputStream(), "utf-8"))){
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                stringBuilder.append(responseLine.trim());
            }
        }
        JsonObject jsonResponse = (JsonObject) jsonParser.parse(stringBuilder.toString());
        JsonArray arr = jsonResponse.getAsJsonArray("suggestions");
        int arrSize = arr.size();
        for(int i = 0; i < arrSize; i++) {
            JsonObject data = (JsonObject) arr.get(i);
            JsonObject obdata = data.getAsJsonObject("data");
            String street = String.valueOf(obdata.get("street_with_type"));
            listStreets.add(street);
        }
        return listStreets;
    }
}
